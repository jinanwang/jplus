package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.Log;
import io.jplus.common.Query;

import java.util.List;

public interface LogService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Log findById(Object id);


    /**
     * find all model
     *
     * @return all <Log
     */
    public List<Log> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Log model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Log model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Log model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Log model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Log> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Log> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Log> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    boolean deleteAll();

    Page<Log> queryPage(Query query);

    public boolean deleteById(Object... ids);
}