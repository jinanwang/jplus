package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.Dept;
import io.jplus.common.Query;

import java.util.List;

public interface DeptService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Dept findById(Object id);


    /**
     * find all model
     *
     * @return all <Dept
     */
    public List<Dept> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Dept model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Dept model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Dept model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Dept model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Dept> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Dept> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Dept> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    public List<Dept> queryList(Query query);

    public Dept findDeptById(Object id);

    public boolean deleteById(Object... ids);
}